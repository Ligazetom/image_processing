#include "FMatrix.h"

FMatrix::FMatrix(){
	data = nullptr;
	columns = 0;
	rows = 0;
}

FMatrix::FMatrix(int columns, int rowsToBe, bool allocate) {
	data = nullptr;
	this->columns = columns;
	this->rows = rowsToBe;
	allocateDataArray(columns);
}

FMatrix::FMatrix(const FVector vec, int evenSide) {
	data = nullptr;
	columns = evenSide;
	rows = evenSide;
	allocateDataArray(evenSide);

	for (int i = 0; i < evenSide; i++) {
		data[i].setLength(evenSide);
	}

	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			data[i].getData()[j] = vec.getConstData()[i * evenSide + j];
		}
	}
}

FMatrix::FMatrix(int columns, int rows) {
	data = nullptr;
	allocateDataArray(columns);

	for (int i = 0; i < columns; i++) {
		data[i].setLength(rows);
	}

	this->columns = columns;
	this->rows = rows;
}

FMatrix::FMatrix(const FMatrix &mat) {
	data = nullptr;
	allocateDataArray(mat.columns);

	for (int i = 0; i < mat.columns; i++) {
		data[i].setLength(mat.data[i].getLength());
		data[i] = mat.data[i];
	}

	columns = mat.columns;
	rows = mat.rows;
}

FMatrix::FMatrix(FMatrix &&mat) {
	data = mat.data;
	mat.data = nullptr;

	columns = mat.columns;
	rows = mat.rows;
}

FMatrix &FMatrix::operator=(const FMatrix &mat) {
	deallocateDataArray();

	if (data == nullptr) {
		allocateDataArray(mat.columns);

		for (int i = 0; i < mat.columns; i++) {
			data[i].setLength(mat.rows);
			data[i] = mat.data[i];
		}
	}

	columns = mat.columns;
	rows = mat.rows;
	
	return *this;
}

FMatrix &FMatrix::operator=(FMatrix &&mat) {
	deallocateDataArray();

	data = mat.data;
	mat.data = nullptr;

	columns = mat.columns;
	rows = mat.rows;

	return *this;
}

FMatrix::~FMatrix(){
	deallocateDataArray();
}

void FMatrix::deallocateDataArray() {
	if (data != nullptr) {
		delete[] data;
	}

	data = nullptr;
}

void FMatrix::allocateDataArray(int count) {
	data = new FVector[count];
}

void FMatrix::copyFImageData(const FImage *image) {
	for (int i = 0; i < image->getFragmentCount(); i++) {
		data[i] = FVector(image, i);
	}
	rows = data[0].getLength();
}

void FMatrix::setImageData(const FImage *image) {
	deallocateDataArray();

	if (data == nullptr) {
		allocateDataArray(image->getFragmentCount());
		copyFImageData(image);
	}

	columns = image->getFragmentCount();
	rows = image->getFragmentImageSize();
}

FMatrix FMatrix::operator*(const FMatrix &mat) const {
	FVector vBuff;
	FMatrix mBuff(mat.columns, data[0].getLength());

	if (columns == mat.rows) {
		for (int i = 0; i < rows; i++) {
			vBuff = rowToVector(i);
			
			for (int j = 0; j < mat.columns; j++) {
				mBuff.data[j].getData()[i] = vBuff * mat.data[j];
			}
		}
	}
	else {
		qDebug() << "Wrong matrix-matrix " << columns << "-" << mat.rows << " size!";
	}

	return mBuff;
}

FVector FMatrix::operator*(const FVector &vec) const {
	FVector vBuff(rows);

	if (columns == vec.getLength()) {
		for (int i = 0; i < rows; i++) {
			vBuff.getData()[i] = rowToVector(i) * vec;
		}
	}
	else {
		qDebug() << "Wrong matrix-vector " << columns << "-" << vec.getLength() << " size!";
	}

	return vBuff;
}

FMatrix FMatrix::operator*(double val) const{
	FMatrix buff(columns, rows);

	for (int i = 0; i < columns; i++) {
		buff.data[i] = data[i] * val;
	}

	return buff;
}

FVector FMatrix::getColumn(int index) const {
	return data[index];
}

FVector FMatrix::rowToVector(int rowIndex) const {
	FVector buff(columns);

	for (int i = 0; i < columns; i++) {
		buff.getData()[i] = data[i].getData()[rowIndex];
	}

	return buff;
}

FVector *FMatrix::getData() {
	return data;
}

FMatrix FMatrix::transpose() const {
	FMatrix mBuff(rows, columns);

	for (int i = 0; i < rows; i++) {
		mBuff.data[i] = rowToVector(i);
	}

	return mBuff;
}

int FMatrix::getColumnCount() const {
	return columns;
}

int FMatrix::getRowCount() const {
	return rows;
}

void FMatrix::print(int size) const {
	QString buff;

	for (int i = 0; i < size; i++) {
		buff = "";

		for (int j = 0; j < size; j++) {
			buff += QString("%1 ").arg(data[j].getData()[i]);
		}
		qDebug() << buff;
	}
}

void FMatrix::print() const {
	QString buff;

	for (int i = 0; i < rows; i++) {
		buff = "";

		for (int j = 0; j < columns; j++) {
			buff += QString("%1 ").arg(data[j].getData()[i]);
		}
		qDebug() << buff;
	}
}

void FMatrix::makeIdentity() {
	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			if (i == j) {
				data[i].getData()[j] = 1.;
			}
			else {
				data[i].getData()[j] = 0.;
			}
		}
	}
}

void FMatrix::swapColumns(int index1, int index2) {
	FVector buff;

	buff = data[index1];
	data[index1] = data[index2];
	data[index2] = buff;
}

void FMatrix::swapRows(int index1, int index2) {
	FVector buff1, buff2;

	buff1 = rowToVector(index1);
	buff2 = rowToVector(index2);

	for (int i = 0; i < buff1.getLength(); i++) {
		data[i].getData()[index1] = buff2.getData()[i];
	}

	for (int i = 0; i < buff1.getLength(); i++) {
		data[i].getData()[index2] = buff1.getData()[i];
	}
}

void FMatrix::setRow(int index, FVector vec) {
	if (vec.getLength() == columns) {
		for (int i = 0; i < columns; i++) {
			data[i].getData()[index] = vec.getData()[i];
		}
	}
	else {
		qDebug() << "Wrong vector - row size!";
	}
}

int FMatrix::determinant() const{
	int det = 0;
	FMatrix subMatrix(columns - 1, rows - 1);
	int subi;

	if (columns == rows && rows == 1) {
		return data[0].getData()[0];
	}

	if (columns != rows) {
		throw EXCEPTIONS::SINGULAR_MATRIX;
	}

	if (columns == 2)
		return ((data[0].getData()[0] * data[1].getData()[1]) - (data[1].getData()[0] * data[0].getData()[1]));
	else {
		for (int x = 0; x < columns; x++) {
			subi = 0;

			for (int i = 0; i < columns; i++){

				for (int j = 1; j < columns; j++) {

					if (x == i) {
 						continue;
					}

					subMatrix.data[subi].getData()[j - 1] = data[i].getData()[j];
				}
				
				if (x != i) {
					subi++;
				}
			}

			det += (pow(-1, x) * data[x].getData()[0] * subMatrix.determinant());
		}
	}
	
	return det;
}

FMatrix FMatrix::makeInverse() {
	FMatrix identity(columns, rows);

	identity.makeIdentity();

	return gaussElimination(identity);
}

FMatrix FMatrix::solveForRightSide(FMatrix R) const {
	return gaussElimination(R);
}

FMatrix FMatrix::gaussElimination(FMatrix R) const {
	double max, buffVal = 0.;
	int maxArg;	
	FMatrix A = copyThisObject();

	/*if (A.determinant() == 0 || A.rows != R.rows) {
		return FMatrix(0, 0);
	}*/

	for (int i = 0; i < A.rows; i++) {
		max = -DBL_MAX;

		for (int j = i; j < A.rows; j++) {
			if (fabs(A.data[i].getData()[j]) > max) {
				max = fabs(A.data[i].getData()[j]);
				maxArg = j;
			}
		}

		A.swapRows(i, maxArg);
		R.swapRows(i, maxArg);
		buffVal = A.data[i].getData()[i];

		for (int k = 0; k < A.rows; k++) {
			A.data[k].getData()[i] /= buffVal;
		}

		for (int k = 0; k < R.columns; k++) {
			R.data[k].getData()[i] /= buffVal;
		}

		for (int k = 0; k < A.rows; k++) {
			if (k == i) {
				continue;
			}

			buffVal = A.data[i].getData()[k];

			A.setRow(k, A.rowToVector(k) - (A.rowToVector(i) * buffVal));
			R.setRow(k, R.rowToVector(k) - (R.rowToVector(i) * buffVal));
		}
	}

	return R;
}

bool FMatrix::isValid() const {
	if (columns == 0 || rows == 0) {
		return false;
	}

	return true;
}

FMatrix FMatrix::copyThisObject() const{
	FMatrix newMat(columns, rows);

	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			newMat.getData()[i].getData()[j] = data[i].getData()[j];
		}
	}

	return newMat;
}

FMatrix FMatrix::kroneckerProduct(const FMatrix &R) const {
	FMatrix product(columns * R.columns, rows * R.rows);
	FMatrix buff(R.columns, R.rows);
	
	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			buff = R * data[i].getData()[j];
			product.insertMatrix(buff, i * R.columns, j * R.rows);
		}
	}

	return product;
}

void FMatrix::insertMatrix(FMatrix mat, int columnIndex, int rowIndex) {
	int subi, subj;
	if (mat.columns > columnIndex + columns || mat.rows > rowIndex + rows) {
		qDebug() << "Unable to insert matrix!";
		return;
	}

	subi = 0;

	for (int i = columnIndex; i < columnIndex + mat.columns; i++) {
		subj = 0;

		for (int j = rowIndex; j < rowIndex + mat.rows; j++) {
			data[i].getData()[j] = mat.data[subi].getData()[subj];
			subj++;
		}

		subi++;
	}
}

void FMatrix::makeAllValuesPositive() {
	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			data[i].getData()[j] = fabs(data[i].getData()[j]);
		}
	}
}

unsigned char* FMatrix::getAllValues() const {
	unsigned char *arr = new unsigned char[columns * rows];
	FVector buff;

	for (int i = 0; i < rows; i++) {
		buff = rowToVector(i);

		for (int j = 0; j < buff.getLength(); j++) {
			arr[i * columns + j] = unsigned char(buff.getData()[j]);
		}
	}

	return arr;
}

const FVector* FMatrix::getConstData() const {
	return data;
}

void FMatrix::setColumnsCount(int count) {
	columns = count;
}

void FMatrix::setData(FVector *dat) {
	data = dat;
}