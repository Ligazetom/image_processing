#pragma once

#include "FImage.h"
#include "qdebug.h"

class FVector
{
public:
	FVector();
	FVector(int length);
	FVector(const FImage *image, int fragmentIndex);
	FVector(const FVector &vec);
	FVector(FVector &&vec);
	FVector &operator=(const FVector &vec);
	FVector &operator=(FVector &&vec);
	~FVector();

	void print() const;
	void setLength(int length);
	int getLength() const;
	double *getData();
	const double* getConstData() const;
	void addData(double val);

	double operator*(const FVector &vec) const;
	FVector operator*(double val) const;
	FVector operator/(double val) const;
	FVector operator+(double val) const;
	FVector operator-(double val) const;
	FVector operator-(const FVector &vec) const;
	FVector operator+(const FVector &vec) const;

	double norm() const;

private:
	double *data;
	int length;

	void allocateDataArray(int length);
	void deallocateDataArray();
	void copyData(unsigned char *arr, int length);
	double scaleToReal(unsigned char val);
};

