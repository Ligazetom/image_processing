#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindow.h"
#include "RenderArea.h"
#include "qfiledialog.h"
#include "qmessagebox.h"
#include <fstream>
#include <string>
#include "FImage.h"
#include "FCore.h"
#include <QObject>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);

private:
	Ui::MainWindowClass ui;
	RenderArea Area;
	FImage BaseImage;
	FImage AproximateImage;
	FCore Core;

	void loadFile(QString filename);
	void loadImage(std::ifstream &stream);
	void updateImage();

	int powerTwo(int i);

private slots:
	void on_actionLoad_Image_triggered();
	void on_pushButtonFragment_pressed();
	void on_horizontalSliderMargin_sliderMoved(int i);
	void on_spinBoxMargin_valueChanged(int i);
	void on_comboBoxFragmentSide_currentIndexChanged(int i);
	void on_pushButtonProcess_pressed();
	void on_checkBoxApp_toggled(bool b);
};
