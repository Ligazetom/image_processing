#pragma once

#include "qwidget.h"
#include "qimage.h"
#include "FPainter.h"
#include "qevent.h"
#include "qpainter.h"
#include "qdebug.h"
#include "FImage.h"
#include <QObject>

class RenderArea : public QWidget
{
	Q_OBJECT

public:
	RenderArea(QWidget *parent = Q_NULLPTR);
	~RenderArea();
	
	void createCanvas(int width, int height);
	void setImage(FImage *image);
	void drawBaseImage();
	void drawFragment(int index);
	void drawFragmentedImage(int margin);
	void cleanCanvas();
protected:
	void paintEvent(QPaintEvent *event);
private:
	FPainter painter;
	QImage *Canvas;
	FImage *BaseImage;
};

