#pragma once

#include "FMath.h"
#include "FVector.h"
#include "FMatrix.h"
#include "qdebug.h"
#include "qthreadpool.h"
#include <thread>

class FCore
{
public:
	FCore();
	~FCore();

	void process(int index, int L);
	void setFragmentData(FImage *image);
	FMatrix* getApproximateImage();
	FMatrix &getX();
	const FMatrix &getDict() const;
	const FMatrix &getFragmentData() const;


private:
	FMatrix fragmentData, approximateData;
	FMatrix dictionary;
	FMatrix X;
	int fragmentSide, height, width;

	void makeAproximateImage();	
	int getMaxThreads() const;
	void joinThreads(std::thread *threads, int max);
};

