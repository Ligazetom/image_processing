/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QAction *actionLoad_Image;
    QAction *actionnew;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBoxSettings;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QSpinBox *spinBoxMargin;
    QLabel *label_2;
    QSlider *horizontalSliderMargin;
    QHBoxLayout *horizontalLayout;
    QLabel *label_4;
    QComboBox *comboBoxFragmentSide;
    QLabel *label_5;
    QPushButton *pushButtonFragment;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_7;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_7;
    QSpinBox *spinBoxHeight;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_8;
    QSpinBox *spinBoxWidth;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QSpinBox *spinBoxFragmentCount;
    QCheckBox *checkBoxApp;
    QGroupBox *groupBoxAlgorithm;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_6;
    QSpinBox *spinBoxL;
    QComboBox *comboBoxAlgorithm;
    QPushButton *pushButtonProcess;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QMenuBar *menuBar;
    QMenu *menuImage;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QStringLiteral("MainWindowClass"));
        MainWindowClass->resize(1151, 740);
        actionLoad_Image = new QAction(MainWindowClass);
        actionLoad_Image->setObjectName(QStringLiteral("actionLoad_Image"));
        actionnew = new QAction(MainWindowClass);
        actionnew->setObjectName(QStringLiteral("actionnew"));
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_8 = new QHBoxLayout(centralWidget);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        groupBoxSettings = new QGroupBox(centralWidget);
        groupBoxSettings->setObjectName(QStringLiteral("groupBoxSettings"));
        verticalLayout_2 = new QVBoxLayout(groupBoxSettings);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(groupBoxSettings);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        spinBoxMargin = new QSpinBox(groupBoxSettings);
        spinBoxMargin->setObjectName(QStringLiteral("spinBoxMargin"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(spinBoxMargin->sizePolicy().hasHeightForWidth());
        spinBoxMargin->setSizePolicy(sizePolicy);
        spinBoxMargin->setMaximum(30);

        horizontalLayout_2->addWidget(spinBoxMargin);

        label_2 = new QLabel(groupBoxSettings);
        label_2->setObjectName(QStringLiteral("label_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(label_2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalSliderMargin = new QSlider(groupBoxSettings);
        horizontalSliderMargin->setObjectName(QStringLiteral("horizontalSliderMargin"));
        sizePolicy.setHeightForWidth(horizontalSliderMargin->sizePolicy().hasHeightForWidth());
        horizontalSliderMargin->setSizePolicy(sizePolicy);
        horizontalSliderMargin->setMaximum(30);
        horizontalSliderMargin->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSliderMargin);


        verticalLayout_2->addLayout(verticalLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_4 = new QLabel(groupBoxSettings);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout->addWidget(label_4);

        comboBoxFragmentSide = new QComboBox(groupBoxSettings);
        comboBoxFragmentSide->setObjectName(QStringLiteral("comboBoxFragmentSide"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(comboBoxFragmentSide->sizePolicy().hasHeightForWidth());
        comboBoxFragmentSide->setSizePolicy(sizePolicy2);

        horizontalLayout->addWidget(comboBoxFragmentSide);

        label_5 = new QLabel(groupBoxSettings);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout->addWidget(label_5);


        verticalLayout_2->addLayout(horizontalLayout);


        verticalLayout_4->addWidget(groupBoxSettings);

        pushButtonFragment = new QPushButton(centralWidget);
        pushButtonFragment->setObjectName(QStringLiteral("pushButtonFragment"));

        verticalLayout_4->addWidget(pushButtonFragment);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_6 = new QVBoxLayout(groupBox);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout_6->addWidget(label_7);

        spinBoxHeight = new QSpinBox(groupBox);
        spinBoxHeight->setObjectName(QStringLiteral("spinBoxHeight"));
        spinBoxHeight->setAlignment(Qt::AlignCenter);
        spinBoxHeight->setReadOnly(true);
        spinBoxHeight->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBoxHeight->setMaximum(99999);
        spinBoxHeight->setSingleStep(0);

        horizontalLayout_6->addWidget(spinBoxHeight);


        horizontalLayout_7->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_5->addWidget(label_8);

        spinBoxWidth = new QSpinBox(groupBox);
        spinBoxWidth->setObjectName(QStringLiteral("spinBoxWidth"));
        spinBoxWidth->setAlignment(Qt::AlignCenter);
        spinBoxWidth->setReadOnly(true);
        spinBoxWidth->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBoxWidth->setMaximum(99999);

        horizontalLayout_5->addWidget(spinBoxWidth);


        horizontalLayout_7->addLayout(horizontalLayout_5);


        verticalLayout_6->addLayout(horizontalLayout_7);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        spinBoxFragmentCount = new QSpinBox(groupBox);
        spinBoxFragmentCount->setObjectName(QStringLiteral("spinBoxFragmentCount"));
        spinBoxFragmentCount->setAlignment(Qt::AlignCenter);
        spinBoxFragmentCount->setReadOnly(true);
        spinBoxFragmentCount->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBoxFragmentCount->setMaximum(9999999);

        horizontalLayout_3->addWidget(spinBoxFragmentCount);


        verticalLayout_6->addLayout(horizontalLayout_3);

        checkBoxApp = new QCheckBox(groupBox);
        checkBoxApp->setObjectName(QStringLiteral("checkBoxApp"));

        verticalLayout_6->addWidget(checkBoxApp);


        verticalLayout_4->addWidget(groupBox);


        verticalLayout_3->addLayout(verticalLayout_4);

        groupBoxAlgorithm = new QGroupBox(centralWidget);
        groupBoxAlgorithm->setObjectName(QStringLiteral("groupBoxAlgorithm"));
        verticalLayout_7 = new QVBoxLayout(groupBoxAlgorithm);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_6 = new QLabel(groupBoxAlgorithm);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_4->addWidget(label_6);

        spinBoxL = new QSpinBox(groupBoxAlgorithm);
        spinBoxL->setObjectName(QStringLiteral("spinBoxL"));
        spinBoxL->setAlignment(Qt::AlignCenter);
        spinBoxL->setButtonSymbols(QAbstractSpinBox::UpDownArrows);
        spinBoxL->setMinimum(1);
        spinBoxL->setMaximum(99999);
        spinBoxL->setSingleStep(50);
        spinBoxL->setValue(50);

        horizontalLayout_4->addWidget(spinBoxL);


        verticalLayout_5->addLayout(horizontalLayout_4);

        comboBoxAlgorithm = new QComboBox(groupBoxAlgorithm);
        comboBoxAlgorithm->setObjectName(QStringLiteral("comboBoxAlgorithm"));
        sizePolicy.setHeightForWidth(comboBoxAlgorithm->sizePolicy().hasHeightForWidth());
        comboBoxAlgorithm->setSizePolicy(sizePolicy);

        verticalLayout_5->addWidget(comboBoxAlgorithm);

        pushButtonProcess = new QPushButton(groupBoxAlgorithm);
        pushButtonProcess->setObjectName(QStringLiteral("pushButtonProcess"));

        verticalLayout_5->addWidget(pushButtonProcess);


        verticalLayout_7->addLayout(verticalLayout_5);


        verticalLayout_3->addWidget(groupBoxAlgorithm);


        horizontalLayout_8->addLayout(verticalLayout_3);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 900, 667));
        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout_8->addWidget(scrollArea);

        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1151, 21));
        menuImage = new QMenu(menuBar);
        menuImage->setObjectName(QStringLiteral("menuImage"));
        MainWindowClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindowClass->setStatusBar(statusBar);

        menuBar->addAction(menuImage->menuAction());
        menuImage->addAction(actionLoad_Image);

        retranslateUi(MainWindowClass);

        comboBoxFragmentSide->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "MainWindow", Q_NULLPTR));
        actionLoad_Image->setText(QApplication::translate("MainWindowClass", "Load Image", Q_NULLPTR));
        actionnew->setText(QApplication::translate("MainWindowClass", "new", Q_NULLPTR));
        groupBoxSettings->setTitle(QApplication::translate("MainWindowClass", "Settings", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindowClass", "Margin:", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindowClass", "px", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindowClass", "Fragment side length:", Q_NULLPTR));
        comboBoxFragmentSide->clear();
        comboBoxFragmentSide->insertItems(0, QStringList()
         << QApplication::translate("MainWindowClass", "1", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "2", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "4", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "8", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "16", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "32", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "64", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "128", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "256", Q_NULLPTR)
        );
        label_5->setText(QApplication::translate("MainWindowClass", "px", Q_NULLPTR));
        pushButtonFragment->setText(QApplication::translate("MainWindowClass", "Fragment Image", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindowClass", "Stats", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindowClass", "Height:", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindowClass", "Width:", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindowClass", "Fragment count:", Q_NULLPTR));
        checkBoxApp->setText(QApplication::translate("MainWindowClass", "Show Approximate Image", Q_NULLPTR));
        groupBoxAlgorithm->setTitle(QApplication::translate("MainWindowClass", "Algorithm", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindowClass", "L:", Q_NULLPTR));
        comboBoxAlgorithm->clear();
        comboBoxAlgorithm->insertItems(0, QStringList()
         << QApplication::translate("MainWindowClass", "Matching Pursuit", Q_NULLPTR)
         << QApplication::translate("MainWindowClass", "Orthogonal Matching Pursuit", Q_NULLPTR)
        );
        pushButtonProcess->setText(QApplication::translate("MainWindowClass", "Process", Q_NULLPTR));
        menuImage->setTitle(QApplication::translate("MainWindowClass", "Image", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
