#pragma once

class FImage
{
public:
	FImage();
	~FImage();

	void setImage(unsigned char *image, int width, int height);
	unsigned char *getMainImage() const;
	unsigned char *getFragmentImage(int index) const;
	int getMainImageSize() const;
	int getMainImageWidth() const;
	int getMainImageHeight() const;
	void setFragmentImageSide(int value);
	int getFragmentImageSide() const;
	int getFragmentCount() const;
	int getFragmentImageSize() const;
	void processImage();
	int getWidth() const;
	int getHeight() const;

private:
	int width, height, size;
	unsigned char *mainImage;
	unsigned char **fragments;
	int fragmentImageSide;
	int fragmentImageSize;
	int fragmentCount;

	void deallocateFragments();
	void allocateFragments();
	int computeFragmentCount();
	void fragmentMainImage();
};

