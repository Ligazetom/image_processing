#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	ui.scrollArea->setWidgetResizable(false);
	ui.groupBoxSettings->setEnabled(false);
	ui.pushButtonFragment->setEnabled(false);
}

void MainWindow::on_actionLoad_Image_triggered() {	
	loadFile(QFileDialog::getOpenFileName(this, "Image", "", "Image Files (*.pgm)"));
	ui.scrollArea->setWidget(&Area);
}

void MainWindow::loadFile(QString filename) {
	std::ifstream stream;
	QMessageBox msg;

	if (filename == "") {
		return;
	}

	stream.open(filename.toStdString(), std::ios::out | std::ios::binary);

	if (stream.is_open()) {
		loadImage(stream);
		stream.close();
	}
	else{
		msg.setText("Unable to open file!");
		msg.exec();
	}	
}

void MainWindow::loadImage(std::ifstream &stream) {
	std::string line;
	unsigned char *imgValues;
	int width, height, size;

	Area.createCanvas(ui.scrollArea->width() - 3, ui.scrollArea->height() - 3);

	std::getline(stream, line);
	
	std::getline(stream, line);
	width = std::atoi(line.c_str());

	std::getline(stream, line);
	height = std::atoi(line.c_str());

	size = width * height;
	imgValues = new unsigned char[size];

	std::getline(stream, line);

	stream.read((char*)imgValues, size);

	BaseImage.setImage(imgValues, width, height);
	Area.setImage(&BaseImage);
	Area.drawBaseImage();
	ui.pushButtonFragment->setEnabled(true);
	ui.spinBoxWidth->setValue(BaseImage.getWidth());
	ui.spinBoxHeight->setValue(BaseImage.getHeight());
}

void MainWindow::on_pushButtonFragment_pressed() {
	updateImage();
	ui.groupBoxSettings->setEnabled(true);
}

void MainWindow::on_horizontalSliderMargin_sliderMoved(int i) {
	ui.spinBoxMargin->setValue(i);
}

void MainWindow::on_spinBoxMargin_valueChanged(int i) {
	ui.horizontalSliderMargin->setValue(i);
	updateImage();
}

void MainWindow::on_comboBoxFragmentSide_currentIndexChanged(int i) {
	BaseImage.setFragmentImageSide(powerTwo(i));
	updateImage();
}

void MainWindow::updateImage() {
	Area.cleanCanvas();
	BaseImage.processImage();
	ui.spinBoxFragmentCount->setValue(BaseImage.getFragmentCount());
	Area.drawFragmentedImage(ui.spinBoxMargin->value());
}

int MainWindow::powerTwo(int i) {
	int x = 1;

	if (i == 0) {
		return x;
	}

	for (int j = 0; j < i; j++) {
		x *= 2;
	}

	return x;
}

void MainWindow::on_pushButtonProcess_pressed() {
	Core.setFragmentData(&BaseImage);
	Core.process(ui.comboBoxAlgorithm->currentIndex(), ui.spinBoxL->value());
	AproximateImage.setImage((*Core.getApproximateImage()).getAllValues(), BaseImage.getWidth(), BaseImage.getHeight());
}

void MainWindow::on_checkBoxApp_toggled(bool b) {
	if (b) {
		Area.setImage(&AproximateImage);
		Area.drawBaseImage();
	}
	else {
		Area.setImage(&BaseImage);
		Area.drawBaseImage();
	}	
}