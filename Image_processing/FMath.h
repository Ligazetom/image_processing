#pragma once

#include "FVector.h"
#include "FMatrix.h"
#include <cmath>
#include <chrono>
#include "qhash.h"

#define TOLERANCE 0.000000005

namespace FMath
{
	FVector matchingPursuit(const FVector &y, const FMatrix &d, int L);
	FVector orthogonalMatchingPursuit(const FVector &y, const FMatrix &D, int L);
	FMatrix optimalDirectionsMethod(int dictSize, FMatrix y, int L);
	
	int numOfZeroValues(FVector &vector, double tolerance);
	int numOfNonZeroValues(FVector &vector, double tolerance);
	FMatrix createRandomNormalDictionary(int rows, int columns);
	FMatrix createRandomNormalNormedDictionary(int rows, int columns);
	FMatrix createSinCosDictionary(int rows, int columns);
	void normalizeMatrixByColumnsL2(FMatrix &Dict);
	double cropValue(double val, double lowLimit, double highLimit);
};

