#include "FCore.h"

void parallelRun(int methodIndex, int indexFrom, int indexTo, FMatrix fragmentData, FMatrix dictionary, int L, FCore *core) {
	FVector(*algorithm)(const FVector &y, const FMatrix &D, int L) = nullptr;
	
	switch (methodIndex) {
	case 0: {
		algorithm = &FMath::matchingPursuit;
		break;
	}
	case 1: {
		algorithm = &FMath::orthogonalMatchingPursuit;
		break;
	}
	}

	for (int i = indexFrom; i < indexTo; i++) {
		qDebug() << "Fragment: " << i;
		core->getX().getData()[i] = algorithm(fragmentData.getConstData()[i], dictionary, L);		
	}
}

FCore::FCore()
{
}


FCore::~FCore()
{
}

void FCore::process(int index, int L) {	
	X = FMatrix(fragmentData.getColumnCount(), fragmentData.getColumnCount());
	int threadCount = getMaxThreads();
	qDebug() << "Pocet vlaken: " << threadCount;
	std::thread *threads = new std::thread[threadCount];

	//dictionary = FMath::createSinCosDictionary(fragmentData.getRowCount(), fragmentData.getColumnCount());
	dictionary = FMath::createRandomNormalNormedDictionary(fragmentData.getRowCount(), fragmentData.getColumnCount());
	FMath::normalizeMatrixByColumnsL2(dictionary);

	for (int i = 0; i < threadCount; i++) {
		threads[i] = std::thread(parallelRun,
			index,
			i * fragmentData.getColumnCount() / threadCount,
			i * fragmentData.getColumnCount() / threadCount + fragmentData.getColumnCount() / threadCount,
			fragmentData,
			dictionary,
			L,
			this
		);
	}

	joinThreads(threads, threadCount);
	delete[] threads;

	qDebug() << "Dlzka: " << X.getData()[0].getLength();
}

void FCore::setFragmentData(FImage *image) {
	fragmentData.setImageData(image);
	fragmentSide = image->getFragmentImageSide();
	height = image->getHeight();
	width = image->getWidth();
}

void FCore::makeAproximateImage() {
	approximateData = FMatrix(width, height);

	for (int i = 0; i < fragmentData.getColumnCount(); i++) {
		approximateData.insertMatrix(FMatrix(dictionary * X.getData()[i], fragmentSide),
			(i % (width / fragmentSide)) * fragmentSide,
			((i * fragmentSide) / width) * fragmentSide
		);
	}

	for (int i = 0; i < approximateData.getColumnCount(); i++) {
		for (int j = 0; j < approximateData.getRowCount(); j++) {
			approximateData.getData()[i].getData()[j] = FMath::cropValue(approximateData.getData()[i].getData()[j], 0, 1);
			approximateData.getData()[i].getData()[j] *= 255;
		}
	}
}

FMatrix* FCore::getApproximateImage() {
	makeAproximateImage();
	return &approximateData;
}

const FMatrix& FCore::getDict() const {
	return dictionary;
}

const FMatrix& FCore::getFragmentData() const {
	return fragmentData;
}

FMatrix& FCore::getX() {
	return X;
}

int FCore::getMaxThreads() const {
	int count = std::thread::hardware_concurrency();

	if (count == 0) {
		return 1;
	}
	else {
		return count;
	}
}

void FCore::joinThreads(std::thread *threads, int max) {
	for (int i = 0; i < max; i++) {
		threads[i].join();
	}
}