#include "RenderArea.h"

RenderArea::RenderArea(QWidget *parent)
	:QWidget(parent) {
	setAttribute(Qt::WA_StaticContents);
	BaseImage = nullptr;
}

RenderArea::~RenderArea()
{
}

void RenderArea::paintEvent(QPaintEvent *event) {
	QPainter paint(this);
	QRect area = event->rect();

	paint.drawImage(area, *Canvas);
}

void RenderArea::drawBaseImage() {
	painter.drawMainImage(BaseImage);
	update();
}

void RenderArea::drawFragment(int index) {
	painter.drawFragment(BaseImage, index);
	update();
}

void RenderArea::setImage(FImage *image) {
	BaseImage = image;
}

void RenderArea::drawFragmentedImage(int margin) {
	painter.drawAllFragments(BaseImage, margin);
	update();
}

void RenderArea::createCanvas(int width, int height) {
	Canvas = new QImage(width, height, QImage::Format_ARGB32_Premultiplied);
	Canvas->fill(Qt::white);

	this->resize(Canvas->size());
	this->setMinimumSize(Canvas->size());

	painter.setImage(Canvas);

	update();
}

void RenderArea::cleanCanvas() {
	Canvas->fill(Qt::white);
	update();
}