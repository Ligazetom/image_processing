#include "FImage.h"

FImage::FImage(){
	fragmentCount = 0;
	fragmentImageSide = 8;
	fragmentImageSize = 64;
	mainImage = nullptr;
	fragments = nullptr;
}

FImage::~FImage(){
	delete[] mainImage;

	deallocateFragments();
}

void FImage::setImage(unsigned char *image, int width, int height) {
	mainImage = image;
	this->width = width;
	this->height = height;
	size = width * height;
}

void FImage::processImage() {
	deallocateFragments();
	fragmentCount = computeFragmentCount();
	allocateFragments();
	fragmentMainImage();
}

int FImage::computeFragmentCount() {
	return (width / fragmentImageSide) * (width / fragmentImageSide);
}

void FImage::deallocateFragments() {
	if (fragments != nullptr && fragments[0] != nullptr) {
		for (int i = 0; i < fragmentCount; i++) {
			delete[] fragments[i];
		}
		delete[] fragments;
	}
	else if (fragments != nullptr && fragments[0] == nullptr) {
		delete[] fragments;
	}

	fragments = nullptr;
}

void FImage::allocateFragments() {
	fragments = new unsigned char*[fragmentCount];

	for (int i = 0; i < fragmentCount; i++) {
		fragments[i] = new unsigned char[fragmentImageSide * fragmentImageSide];
	}
}

void FImage::fragmentMainImage() {	
	int arg;
	int fragmentsInRow = width / fragmentImageSide;
	int pixelsInFragmentRow = width * fragmentImageSide;
	int k, l;	 

	for (int i = 0; i < fragmentCount; i++) {
		for (int j = 0; j < fragmentImageSize; j++) {
			k = (i / fragmentsInRow) * pixelsInFragmentRow + (i % fragmentsInRow) * fragmentImageSide;
			l = j % fragmentImageSide + (j / fragmentImageSide) * width;
			
			fragments[i][j] = mainImage[k + l];
		}
	}
}

unsigned char *FImage::getMainImage() const {
	return mainImage;
}

int FImage::getMainImageSize() const {
	return size;
}

unsigned char *FImage::getFragmentImage(int index) const {
	return fragments[index];
}

int FImage::getMainImageHeight() const {
	return height;
}

int FImage::getMainImageWidth() const {
	return width;
}

void FImage::setFragmentImageSide(int value) {
	fragmentImageSide = value;
	fragmentImageSize = value * value;
	processImage();
}

int FImage::getFragmentImageSide() const {
	return fragmentImageSide;
}

int FImage::getFragmentCount() const {
	return fragmentCount;
}

int FImage::getFragmentImageSize() const {
	return fragmentImageSize;
}

int FImage::getWidth() const {
	return width;
}

int FImage::getHeight() const {
	return height;
}