#include "FPainter.h"

FPainter::FPainter(){
}


FPainter::~FPainter()
{
}

bool FPainter::isPixelInImage(int x, int y) {
	if (x < 0 || y < 0) {
		return false;
	}

	if (x > imgWidth - 1 || y > imgHeight - 1) {
		return false;
	}

	return true;
}

void FPainter::setPixel(int x, int y, QColor color) {
	if (isPixelInImage(x, y)) {
		img[x + y * imgWidth] = color.rgba();
	}
}

void FPainter::setImage(QImage *image) {
	img = (QRgb *)image->bits();
	imgWidth = image->width();
	imgHeight = image->height();

	center.setX(imgWidth / 2);
	center.setY(imgHeight / 2);
}

void FPainter::drawMainImage(FImage *image) {
	int width = image->getMainImageWidth();
	int height = image->getMainImageHeight();
	int x, y;

	for (int i = 0; i < image->getMainImageSize(); i++) {
		x = (i % width) - width / 2 + center.x();
		y = (i / width) - height / 2 + center.y();
		setPixel( x, y, rgbFromValue( (image->getMainImage())[i] ) );
	}
}

QColor FPainter::rgbFromValue(unsigned char value) {
	return QColor(value, value, value);
}

void FPainter::drawFragment(FImage *image, int index) {
	int side = image->getFragmentImageSide();
	int x, y;

	if (index >= 0 && index < image->getFragmentCount()) {
		for (int i = 0; i < image->getFragmentImageSize(); i++) {
			x = (i % side) - side / 2 + center.x();
			y = (i / side) - side / 2 + center.y();
			setPixel(x, y, rgbFromValue((image->getFragmentImage(index)[i])));
		}
	}
}

void FPainter::drawAllFragments(FImage *image, int margin) {
	int numOfFragmentsOnSide = image->getMainImageWidth() / image->getFragmentImageSide();
	int pixelsInRow = numOfFragmentsOnSide * image->getFragmentImageSide() + (numOfFragmentsOnSide - 1) * margin;
	int fragmentSide = image->getFragmentImageSide();
	int fragSideWithMargin = fragmentSide + margin;
	int index, pixel;
	int newSide = numOfFragmentsOnSide * fragmentSide + (numOfFragmentsOnSide - 1) * margin;
	int x = center.x() - newSide / 2;
	int y = center.y() - newSide / 2;

	for (int i = 0; i < numOfFragmentsOnSide; i++) {
		for (int j = 0; j < fragmentSide + margin; j++) {
			if (j >= fragmentSide) {
				if (i == numOfFragmentsOnSide - 1) {
					break;
				}

				/*for (int k = 0; k < pixelsInRow; k++) {
					setPixel(x + k, y + i * fragSideWithMargin + j, Qt::white);
				}*/
			}
			else {
				for (int k = 0; k < pixelsInRow; k++) {
					//if (k % fragSideWithMargin >= fragmentSide) {
						//setPixel(x + k, y + i * fragSideWithMargin + j, Qt::white);
					//}
					if(k % fragSideWithMargin < fragmentSide) {
						index = i * numOfFragmentsOnSide + k / fragSideWithMargin;
						pixel = k - (k / fragSideWithMargin) * fragSideWithMargin + j * fragmentSide;

						setPixel(x + k, y + i * fragSideWithMargin + j,	rgbFromValue(image->getFragmentImage(index)[pixel]));
					}
				}
			}
		}
	}
	for (int k = 0; k < pixelsInRow; k++) {
		//setPixel(k, 196, Qt::black);
	}
}