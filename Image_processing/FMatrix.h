#pragma once

#include "FVector.h"
#include "FImage.h"
#include "qdebug.h"
#include "qstring.h"

enum EXCEPTIONS {
	SINGULAR_MATRIX = 0,
};

class FMatrix
{
public:
	FMatrix();
	FMatrix(const FVector vec, int evenSide);
	FMatrix(int columns, int rows);
	FMatrix(int columns, int rowsToBe, bool allocate);
	FMatrix(const FMatrix &mat);
	FMatrix(FMatrix &&mat);
	FMatrix& operator=(const FMatrix &mat);
	FMatrix& operator=(FMatrix &&mat);
	~FMatrix();

	int getColumnCount() const;
	int getRowCount() const;
	void setImageData(const FImage *image);
	FMatrix operator*(const FMatrix &mat) const;
	FVector operator*(const FVector &vec) const;
	FMatrix operator*(double val) const;
	FVector getColumn(const int index) const;
	FVector *getData();
	const FVector* getConstData() const;
	FMatrix transpose() const;
	FVector rowToVector(int rowIndex) const;
	void makeIdentity();
	void swapColumns(int index1, int index2);
	void swapRows(int index1, int index2);
	void setRow(int index, FVector vec);
	int determinant() const;
	void print(int size) const;
	void print() const;
	FMatrix makeInverse();
	FMatrix solveForRightSide(FMatrix R) const;
	bool isValid() const;
	FMatrix kroneckerProduct(const FMatrix &R) const;
	void makeAllValuesPositive();
	unsigned char* getAllValues() const;	
	void insertMatrix(FMatrix mat, int columnIndex, int rowIndex);
	void setColumnsCount(int count);
	void setData(FVector *dat);

private:
	FVector *data;
	int columns;
	int rows;

	FMatrix gaussElimination(FMatrix R) const;
	void deallocateDataArray();
	void allocateDataArray(int count);
	void copyFImageData(const FImage *image);
	FMatrix copyThisObject() const;	
};

