#include "FMath.h"

int maxArgOfDotProduct(const FMatrix &D, FVector &r, QHash<int, int> &Omega) {
	double max = 0.;
	int iBuf = 0;
	double buff;
	QHash<int, int>::iterator it = Omega.begin();

	for (int i = 0; i < Omega.size(); i++) {		
		buff = std::fabs(D.getConstData()[it.value()] * r);
		it++;

		if (buff > max) {
			max = buff;
			iBuf = i;
		}
	}

	return iBuf;
}

double frobeniusNorm(FMatrix A) {
	double norm = 0;

	for (int i = 0; i < A.getColumnCount(); i++) {
		for (int j = 0; j < A.getRowCount(); j++) {
			norm += A.getConstData()[i].getConstData()[j] * A.getConstData()[i].getConstData()[j];
		}
	}

	return norm;
}

double lpNorm(FVector vec, double p) {
	double buff = 0;

	if (p < 1) {
		qDebug() << "Wrong p in lpNorm!";
		return 0;
	}

	for (int i = 0; i < vec.getLength(); i++) {
		buff += pow(fabs(vec.getConstData()[i]), p);
	}

	return pow(buff, 1 / p);
}

const FMatrix indexOmegaUnion(const FMatrix &D, const FVector &omega, int index) {
	FMatrix buff(omega.getLength() + 1, D.getRowCount(), false);
	bool sameIndex = false;
	buff.setColumnsCount(omega.getLength());
	
	for (int i = 0; i < omega.getLength(); i++) {
		if (index == int(omega.getConstData()[i])) {
			sameIndex = true;
		}

		buff.getData()[i] = D.getConstData()[(int)omega.getConstData()[i]];
	}

	if (!sameIndex) {
		buff.getData()[omega.getLength()] = D.getConstData()[index];
		buff.setColumnsCount(omega.getLength() + 1);
	}

	return buff;
}


FMatrix vectorIndexUnion(const FMatrix &D, const FVector &omega) {
	FMatrix buff = FMatrix(omega.getLength(), D.getRowCount());

	for (int i = 0; i < omega.getLength(); i++) {
		buff.getData()[i] = D.getConstData()[int(omega.getConstData()[i])];
	}

	return buff;
}

double lsm(const FVector &y, const FMatrix &D, int index, const FVector &omega) {
	//auto start = std::chrono::high_resolution_clock::now();
	FMatrix newD = indexOmegaUnion(D, omega, index);
	FMatrix transp = newD.transpose();
	//auto finish = std::chrono::high_resolution_clock::now();
	//auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
	//qDebug() << "Index union: " << microseconds.count() / 1000.;

	return sqrt(lpNorm((y - newD * ((transp * newD).makeInverse()) * transp * y), 2));
}

int minArgLsm(const FVector &y, const FMatrix &D, const FVector &omega, QHash<int, int> &omegaC) {
	double min = DBL_MAX;
	int iBuf = 0;
	double buff;
	QHash<int, int>::iterator it = omegaC.begin();
	//FVector omegaC = makeVectorIndexComplement(omega, D.getColumnCount());

	for (int i = 0; i < omegaC.size(); i++) {
		//auto start = std::chrono::high_resolution_clock::now();
	
		buff = lsm(y, D, (it + i).value(), omega);

		//auto finish = std::chrono::high_resolution_clock::now();
		//auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
		//qDebug() << "Lsm: " << microseconds.count() / 1000.;

		if (buff < min) {
			min = buff;
			iBuf = (it + i).value();
		}
	}
	
	return iBuf;	
}

int FMath::numOfZeroValues(FVector &vector, double tolerance) {
	int buff = 0;
	double val;

	for (int i = 0; i < vector.getLength(); i++) {
		val = vector.getData()[i];

		if (std::fabs(val) <= 0. + tolerance) {
			buff++;
		}
	}

	return buff;
}


int FMath::numOfNonZeroValues(FVector &vector, double tolerance) {
	int count = numOfZeroValues(vector, tolerance);

	return vector.getLength() - count;
}

QHash<int, int> makeIndexHash(int size) {
	QHash<int, int> omega;

	for (int i = 0; i < size; i++) {
		omega[i] = i;
	}

	return omega;
}

void setVectorValuesFromOmega(FVector &x, const FVector &y, const FVector &omega) {
	for (int i = 0; i < y.getLength(); i++) {
		x.getData()[(int)(omega.getConstData()[i])] = y.getConstData()[i];
	}
}

FVector FMath::orthogonalMatchingPursuit(const FVector &y, const FMatrix &D, int L) {
	int index;
	FVector omega(0);
	QHash<int, int> omegaC = makeIndexHash(D.getColumnCount());
	FVector x(D.getColumnCount());
	FMatrix newD;

	/*auto start = std::chrono::high_resolution_clock::now();
	auto finish = std::chrono::high_resolution_clock::now();
	auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
	qDebug() << "" << microseconds.count()/1000.;*/

	auto start = std::chrono::high_resolution_clock::now();
	
	for (int i = 0; i < L; i++) {
		//auto start1 = std::chrono::high_resolution_clock::now();
		index = minArgLsm(y, D, omega, omegaC);
		//auto finish = std::chrono::high_resolution_clock::now();
	//	auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start1);
		//qDebug() << "Minimalizacia:" << microseconds.count() / 1000.;

		omega.addData(index);
		omegaC.erase(omegaC.find(index));

		//start1 = std::chrono::high_resolution_clock::now();
		newD = vectorIndexUnion(D, omega);
		//finish = std::chrono::high_resolution_clock::now();
		//microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start1);
		//qDebug() << "Skladanie mnozin:" << microseconds.count() / 1000.;
		FMatrix transp = newD.transpose();

		//start1 = std::chrono::high_resolution_clock::now();
		setVectorValuesFromOmega(x, (transp * newD).makeInverse() * transp * y, omega);
	//	finish = std::chrono::high_resolution_clock::now();
	//	microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start1);
	//	qDebug() << "Pocitanie X:" << microseconds.count() / 1000.;
	}
	auto finish = std::chrono::high_resolution_clock::now();
	auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
	qDebug() << "Fragment time:" << microseconds.count() / 1000.;
	return x;
}

FVector FMath::matchingPursuit(const FVector &y, const FMatrix &D, int L) {
	FVector r = y;
	FVector x(D.getColumnCount());
	QHash<int, int> omega = makeIndexHash(D.getColumnCount());
	int iBuf;	

	for(int iter = 1; iter <= L; iter++) {
		iBuf = maxArgOfDotProduct(D, r, omega);
		x.getData()[iBuf] = D.getConstData()[iBuf] * r;
		r = r - (D.getConstData()[iBuf] * x.getConstData()[iBuf]);
		omega.erase(omega.find(iBuf));
	}

	return x;
}

FMatrix FMath::createRandomNormalDictionary(int rows, int columns) {
	FMatrix buff(columns, rows);

	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			buff.getData()[i].getData()[j] = rand() / (double)(RAND_MAX);
		}
	}

	return buff;
}

FMatrix FMath::createRandomNormalNormedDictionary(int rows, int columns) {
	FMatrix buff(columns, rows);

	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			buff.getData()[i].getData()[j] = rand() / (double)(RAND_MAX);
		}
	}

	return buff;
}

FMatrix FMath::createSinCosDictionary(int rows, int columns) {
	FMatrix buff(columns, rows);

	for (int i = 0; i < columns; i++) {
		for (int j = 0; j < rows; j++) {
			buff.getData()[i].getData()[j] = std::fabs(cos(i) * sin(j + 1));
		}
	}

	return buff;
}

int minArgOD(FVector x, FVector y, FMatrix dict) {
	return 0;
}

FMatrix FMath::optimalDirectionsMethod(int dictSize, FMatrix y, int L) {
	FMatrix D = createRandomNormalNormedDictionary(dictSize, dictSize);
	D.makeAllValuesPositive();

	/*Normovanie aby maximalny prvok bol prave 1*/

	while (true) {
		for (int i = 0; i < dictSize; i++) {

		}
	}
	
}

void FMath::normalizeMatrixByColumnsL2(FMatrix &Dict) {
	for (int i = 0; i < Dict.getColumnCount(); i++) {
		Dict.getData()[i] = Dict.getData()[i] / Dict.getData()[i].norm();
	}
}

double FMath::cropValue(double val, double lowLimit, double highLimit) {
	if (val < lowLimit) {
		return lowLimit;
	}
	else if (val > highLimit) {
		return highLimit;
	}

	return val;
}