#pragma once

#include "qimage.h"
#include "FImage.h"
#include "qdebug.h"
#include "qpoint.h"

class FPainter
{
public:
	FPainter();
	~FPainter();
	void setImage(QImage *image);
	void drawMainImage(FImage *image);
	void drawFragment(FImage *image, int index);
	void drawAllFragments(FImage *image, int margin);
private:
	QPoint center;
	QRgb *img;
	int imgWidth, imgHeight;
	int baseWidth, baseHeight;
	bool isPixelInImage(int x, int y);
	void setPixel(int x, int y, QColor color);
	QColor rgbFromValue(unsigned char value);
};

