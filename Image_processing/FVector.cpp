#include "FVector.h"

FVector::FVector(){
	data = nullptr;
	length = 0;
}

FVector::FVector(int length) {
	data = nullptr;
	this->length = length;

	allocateDataArray(length);

	for (int i = 0; i < length; i++) {
		data[i] = 0.;
	}
}

FVector::FVector(const FImage *image, int fragmentIndex) {
	data = nullptr;
	length = image->getFragmentImageSize();

	allocateDataArray(length);
	copyData(image->getFragmentImage(fragmentIndex), length);
}

FVector::FVector(const FVector &vec) {
	allocateDataArray(vec.length);
	length = vec.length;

	for (int i = 0; i < length; i++) {
		data[i] = vec.data[i];
	}
}

FVector::FVector(FVector &&vec) {
	data = vec.data;
	vec.data = nullptr;
	length = vec.length;
}

FVector &FVector::operator=(const FVector &vec) {
	deallocateDataArray();

	if (data == nullptr) {
		allocateDataArray(vec.length);
		
		for (int i = 0; i < vec.length; i++) {
			data[i] = vec.data[i];
		}
		length = vec.length;
	}

	return *this;
}

FVector &FVector::operator=(FVector &&vec) {
	deallocateDataArray();

	if (data == nullptr) {
		data = vec.data;
		vec.data = nullptr;
		length = vec.length;
	}

	return *this;
}

FVector::~FVector(){
	deallocateDataArray();
}

void FVector::allocateDataArray(int length) {
	data = new double[length];
}

void FVector::deallocateDataArray() {
	if (data != nullptr) {
		delete[] data;
	}

	data = nullptr;
}

double FVector::scaleToReal(unsigned char val) {
	return val / 255.;
}

void FVector::copyData(unsigned char *arr, int length) {
	for (int i = 0; i < length; i++) {
		data[i] = scaleToReal(arr[i]);
	}
}

int FVector::getLength() const {
	return length;
}

double FVector::operator*(const FVector &vec) const {
	double buff = 0;

	if (vec.getLength() == length) {
		for (int i = 0; i < length; i++) {
 			buff += data[i] * vec.data[i];
		}
	}
	else {
		qDebug() << "Wrong vector-vector " << length << "-" << vec.length << " size!";
		qDebug() << this << " " << &vec;
	}

	return buff;
}

double *FVector::getData() {
	return data;
}

void FVector::setLength(int length) {
	deallocateDataArray();
	allocateDataArray(length);
	this->length = length;
}

FVector FVector::operator*(double val) const {
	FVector buff(length);

	for (int i = 0; i < length; i++) {
		buff.data[i] = data[i] * val;
	}

	return buff;
}

FVector FVector::operator/(double val) const {
	FVector buff(length);

	for (int i = 0; i < length; i++) {
		buff.data[i] = data[i] / val;
	}

	return buff;
}

FVector FVector::operator-(const FVector &vec) const {
	FVector buff(length);

	for (int i = 0; i < length; i++) {
		buff.data[i] = data[i] - vec.data[i];
	}

	return buff;
}

FVector FVector::operator+(const FVector &vec) const {
	FVector buff(length);
	
	for (int i = 0; i < length; i++) {
		buff.data[i] = data[i] + vec.data[i];
	}

	return buff;
}

void FVector::print() const {
	/*QString str;

	for (int i = 0; i < length; i++) {
		str += QString("%1 ").arg(data[i]);
	}
	qDebug() << str;*/

	for (int i = 0; i < length; i++) {
		qDebug() << data[i];
	}

	qDebug() << "Length: " << length;
}

double FVector::norm() const {
	double buff = 0;

	for (int i = 0; i < length; i++) {
		buff += (data[i] * data[i]);
	}

	return sqrt(buff);
}

FVector FVector::operator+(double val) const {
	FVector buff(length);

	for (int i = 0; i < length; i++) {
		buff.data[i] = data[i] + val;
	}

	return buff;
}

FVector FVector::operator-(double val) const {
	FVector buff(length);

	for (int i = 0; i < length; i++) {
		buff.data[i] = data[i] - val;
	}

	return buff;
}

void FVector::addData(double val) {
	double *newData = new double[length + 1];

	for (int i = 0; i < length; i++) {
		newData[i] = data[i];
	}

	newData[length] = val;

	deallocateDataArray();

	data = newData;
	length++;
}

const double* FVector::getConstData() const {
	return data;
}